from sql_alchemy import local

class MultasModel(local.Model):
    __tablename__ = 'tb_multas'

    id_multa = local.Column(local.Integer, primary_key=True)
    id_consulta = local.Column(local.Integer, local.ForeignKey('tb_consultas.id_consulta'))

    def __init__(self, id_multa, dados):
        self.id_multa = id_multa
        self.id_consulta = dados['id_consulta']

    def json(self):
        return {
            'id_multa': self.id_multa,
            'id_consulta': self.id_consulta
        }

    def salvar_multa(self):
        local.session.add(self)
        local.session.commit()