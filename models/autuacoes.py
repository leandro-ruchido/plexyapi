from sql_alchemy import local

class AutuacoesModel(local.Model):
    __tablename__ = 'tb_autuacoes'

    id_autuacao = local.Column(local.Integer, primary_key=True)
    id_consulta = local.Column(local.Integer, local.ForeignKey('tb_consultas.id_consulta'))

    def __init__(self, **dados):
        self.id_consulta = dados['id_consulta']

    def json(self):
        return {
            'id_autuacao': self.id_autuacao,
            'id_consulta': self.id_consulta
        }

    def salvar_autuacao(self):
        local.session.add(self)
        local.session.commit()