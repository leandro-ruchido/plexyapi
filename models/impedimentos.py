from sql_alchemy import local

class ImpedimentosModel(local.Model):
    __tablename__ = 'tb_impedimentos'

    id_impedimento = local.Column(local.Integer, primary_key=True)
    id_consulta = local.Column(local.Integer, local.ForeignKey('tb_consultas.id_consulta'))

    def __init__(self, id_impedimento, **dados):
        self.id_impedimento = id_impedimento
        self.id_consulta = dados['id_consulta']

    def json(self):
        return {
            'id_impedimento': self.id_impedimento,
            'id_consulta': self.id_consulta
        }

    def salvar_impedimento(self):
        local.session.add(self)
        local.session.commit()