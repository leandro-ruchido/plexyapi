from sql_alchemy import local

class ConsultasModel(local.Model):
    __tablename__ = 'tb_consultas'

    id_consulta = local.Column(local.Integer, primary_key=True, autoincrement=True)
    placa = local.Column(local.String(100))
    renavam = local.Column(local.String(100))
    # placaAnterior = local.Column(local.String(100))
    # municipio = local.Column(local.String(100))
    # municipioAnterior = local.Column(local.String(100))
    # marca = local.Column(local.String(100))
    # anoFabricacao = local.Column(local.String(100))
    # anoModelo = local.Column(local.String(100))
    # ipvaPago = local.Column(local.String(100))
    # parcelaIpva = local.Column(local.String(100))
    # seguroPago = local.Column(local.String(100))
    # parcelaSeguro = local.Column(local.String(100))
    # seguroAnteriorPago = local.Column(local.String(100))
    # taxaLicenciamentoPaga = local.Column(local.String(100))
    # dataLicenciamento = local.Column(local.String(100))
    # situacaoLicenciamento = local.Column(local.String(200))
    # numeroAr = local.Column(local.String(100))
    # comunicado = local.Column(local.String(1000))
    # mensagemPendencias = local.Column(local.String(1000))
    # mensagemAutuacoes = local.Column(local.String(200))
    # mensagemMultas = local.Column(local.String(200))

    # def __init__(self, placa, renavam, placaAnterior, municipio, municipioAnterior, marca, anoFabricacao, anoModelo,
    #              ipvaPago, parcelaIpva, seguroPago, parcelaSeguro, seguroAnteriorPago, taxaLicenciamentoPaga,
    #              dataLicenciamento,
    #              situacaoLicenciamento, numeroAr, comunicado, mensagemPendencias, mensagemAutuacoes, mensagemMultas):
    def __init__(self, placa, renavam):
        self.placa = placa
        self.renavam = renavam
        # self.placaAnterior = placaAnterior
        # self.municipio = municipio
        # self.municipioAnterior = municipioAnterior
        # self.marca = marca
        # self.anoFabricacao = anoFabricacao
        # self.anoModelo = anoModelo
        # self.ipvaPago = ipvaPago
        # self.parcelaIpva = parcelaIpva
        # self.seguroPago = seguroPago
        # self.parcelaSeguro = parcelaSeguro
        # self.seguroAnteriorPago = seguroAnteriorPago
        # self.taxaLicenciamentoPaga = taxaLicenciamentoPaga
        # self.dataLicenciamento = dataLicenciamento
        # self.situacaoLicenciamento = situacaoLicenciamento
        # self.numeroAr = numeroAr
        # self.comunicado = comunicado
        # self.mensagemPendencias = mensagemPendencias
        # self.mensagemAutuacoes = mensagemAutuacoes
        # self.mensagemMultas = mensagemMultas

    def json(self):
        return {
            'id_consulta': self.id_consulta,
            'placa': self.placa,
            'renavam': self.renavam
            # 'placaAnterior': self.placaAnterior,
            # 'municipio': self.municipio,
            # 'municipioAnterior': self.municipioAnterior,
            # 'marca': self.marca,
            # 'anoFabricacao': self.anoFabricacao,
            # 'anoModelo': self.anoModelo,
            # 'ipvaPago': self.ipvaPago,
            # 'parcelaIpva': self.parcelaIpva,
            # 'seguroPago': self.seguroPago,
            # 'parcelaSeguro': self.parcelaSeguro,
            # 'seguroAnteriorPago': self.seguroAnteriorPago,
            # 'taxaLicenciamentoPaga': self.taxaLicenciamentoPaga,
            # 'dataLicenciamento': self.dataLicenciamento,
            # 'situacaoLicenciamento': self.situacaoLicenciamento,
            # 'numeroAr': self.numeroAr,
            # 'comunicado': self.comunicado,
            # 'mensagemPendencias': self.mensagemPendencias,
            # 'impedimentosRestricoes': [],
            # 'mensagemAutuacoes': self.mensagemAutuacoes,
            # 'autuacoes': [],
            # 'mensagemMultas': self.mensagemMultas,
            # 'multas': []
        }

    @classmethod
    def find_consulta(cls, placa, renavam):
        consulta = cls.query.filter_by(placa=placa, renavam=renavam).first()
        if consulta:
            return consulta
        return None

    def salvar_consulta(self):
        placa = self[0]
        renavam = self[1]
        print("placa:" + placa)
        print("renavam:" + renavam)
        consulta = "INSERT INTO tb_consultas VALUES ('" + placa + "','" + renavam + "')"
        print(consulta)
        local.session.add(consulta)
        local.session.commit()
        # local.session.add(self)
        # local.session.commit()