import sqlite3

connection = sqlite3.connect('local.db')
cursor = connection.cursor()

# criar_tb_consulta = "CREATE TABLE IF NOT EXISTS tb_consultas ( \
#                    id_consulta INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, \
#                    placa text, \
#                    renavam text, \
#                    placaAnterior text, \
#                    municipio text, \
#                    municipioAnterior text, \
#                    marca text, \
#                    anoFabricacao text, \
#                    anoModelo text, \
#                    ipvaPago text, \
#                    parcelaIpva text, \
#                    seguroPago text, \
#                    parcelaSeguro text, \
#                    seguroAnteriorPago text, \
#                    taxaLicenciamentoPaga text, \
#                    dataLicenciamento text, \
#                    situacaoLicenciamento text, \
#                    numeroAr text, \
#                    comunicado text, \
#                    mensagemPendencias text, \
#                    mensagemAutuacoes text, \
#                    mensagemMultas text \
#                    # )"
criar_tb_consulta2 = "CREATE TABLE IF NOT EXISTS tb_consultas (id_consulta INTEGER PRIMARY KEY AUTOINCREMENT, placa text, renavam text)"
# criar_tb_autuacoes = "CREATE TABLE IF NOT EXISTS tb_autuacoes ( \
#                      id_autuacao INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, \
#                      orgao text, \
#                      placa text, \
#                      descricaoSituacao text, \
#                      marca text, \
#                      codigo text, \
#                      data text, \
#                      hora text, \
#                      descricaoInfracao text, \
#                      endereco text, \
#                      municipio text, \
#                      dataInclusaoInfracao text, \
#                      dataLimiteDefesa text, \
#                      aitNotificacao text, \
#                      numeroProcessamento text, \
#                      id_consulta_fk INTEGER NOT NULL FOREING KEY \
#                      )"
# criar_tb_multas = "CREATE TABLE IF NOT EXISTS tb_multas ( \
#                   id_multa INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, \
#                   orgao text, \
#                   placa text, \
#                   descricaoSituacao text, \
#                   marca text, \
#                   codigo text, \
#                   data text, \
#                   hora text, \
#                   descricaoInfracao text, \
#                   endereco text, \
#                   municipio text, \
#                   dataInclusaoInfracao text, \
#                   dataLimiteRecurso text, \
#                   aitNotificacao text, \
#                   numeroProcessamento text, \
#                   valor text, \
#                   id_consulta_fk INTEGER NOT NULL \
#                   )"
# criar_tb_impedimentos = "CREATE TABLE IF NOT EXISTS tb_impedimentos ( \
#                         id_impedimento INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, \
#                         id_consulta_fk INTEGER NOT NULL \
#                         )"

cursor.execute(criar_tb_consulta2)
# cursor.execute(criar_tb_autuacoes)
# cursor.execute(criar_tb_multas)
# cursor.execute(criar_tb_impedimentos)

connection.commit()
connection.close()