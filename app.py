from flask import Flask
from flask_restful import Api
from resources.plexy import Plexy, PlexyInfo
from resources.consultas import Consultas

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///local.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
api = Api(app)


@app.before_first_request
def criar_banco():
    local.create_all()


api.add_resource(Plexy, '/plexy')
api.add_resource(PlexyInfo, '/plexy/mg')
api.add_resource(Consultas, '/plexy/responsemg')

if __name__ == '__main__':
    from sql_alchemy import local
    local.init_app(app)
    app.run(debug=True)
