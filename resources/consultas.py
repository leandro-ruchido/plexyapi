from flask_restful import Resource, reqparse
from models.plexyresponse import PlexyresponseModel
from models.consultas import ConsultasModel

class Consultas(Resource):
    atributos = reqparse.RequestParser()
    # atributos.add_argument('id_consulta', type=int, primary_key=True, auto_increment=True)
    atributos.add_argument('placa', type=str)
    atributos.add_argument('renavam', type=str)
    atributos.add_argument('placaAnterior', type=str)
    atributos.add_argument('municipio', type=str)
    atributos.add_argument('municipioAnterior', type=str)
    atributos.add_argument('marca', type=str)
    atributos.add_argument('anoFabricacao', type=str)
    atributos.add_argument('anoModelo', type=str)
    atributos.add_argument('ipvaPago', type=str)
    atributos.add_argument('parcelaIpva', type=str)
    atributos.add_argument('seguroPago', type=str)
    atributos.add_argument('parcelaSeguro', type=str)
    atributos.add_argument('seguroAnteriorPago', type=str)
    atributos.add_argument('taxaLicenciamentoPaga', type=str)
    atributos.add_argument('dataLicenciamento', type=str)
    atributos.add_argument('situacaoLicenciamento', type=str)
    atributos.add_argument('numeroAr', type=str)
    atributos.add_argument('comunicado', type=str)
    atributos.add_argument('mensagemPendencias', type=str)
    atributos.add_argument('impedimentosRestricoes', action='append', type=dict)
    atributos.add_argument('mensagemAutuacoes', type=str)
    atributos.add_argument('autuacoes', action='append', type=dict)
    atributos.add_argument('mensagemMultas', type=str)
    atributos.add_argument('multas', action='append', type=dict)

    def post(self):
        print('=============================================================================')
        print('DADOS RECEBIDOS FORMATO JSON')
        dados = Consultas.atributos.parse_args()
        plexy_objeto = PlexyresponseModel(**dados)
        request_dados = plexy_objeto.json()

        nova_consulta = []
        nova_consulta.append(request_dados['placa'])
        nova_consulta.append(request_dados['renavam'])
        print('NOVA CONSULTA')
        try:
            print(nova_consulta)
            ConsultasModel.salvar_consulta(nova_consulta)
            return {'message': 'Data received sucessfully!'}, 201
        except:
            return {'message': 'Internal error ocurred!'}, 500
        print('=============================================================================')