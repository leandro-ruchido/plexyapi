from flask_restful import Resource, reqparse
from models.plexy import PlexyModel
import requests
import time

class Plexy(Resource):
    def get(self):
        return {'plexi': 'Get Plexy.'}

class PlexyInfo(Resource):
    atributos = reqparse.RequestParser()
    atributos.add_argument('placa', required=True, type=str, help="The field 'placa' is required")
    atributos.add_argument('chassi', required=True, type=str, help="The field 'chassi' is required")

    def post(self):
        dados = PlexyInfo.atributos.parse_args()
        plexy_objeto_mg = PlexyModel(dados['placa'], dados['chassi'])
        request_dados_mg = plexy_objeto_mg.json()

        url_plexy_mg = 'https://api.plexi.com.br//api/maestro/detran-mg/veiculos/situacao-veiculo'
        api_key_plexy = '90vfG3mGDisIqKYEvqg69eb7i9qyJCATIj98SmIZzjmD9m5J8FDwH7gytuGsoSeCbwIssYY2RvWJBkRjzal0RSDIKPLft2DrLn1X'

        headers_plexi_mg = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + api_key_plexy
        }

        body_plexi = {
            'placa': request_dados_mg['placa'],
            'chassi': request_dados_mg['chassi']
        }

        resposta_plexi = requests.request('POST', url_plexy_mg, json=body_plexi, headers=headers_plexi_mg)

        # Coletou a request Id da requisição
        request_id = resposta_plexi.json()

        #GET
        headers_plexi_get = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + api_key_plexy
        }

        response_url = 'https://api.plexi.com.br/api/maestro/result/' + request_id['requestId']

        resposta_plexi = requests.request('GET', response_url, headers=headers_plexi_get)

        while resposta_plexi.status_code == 202:
            time.sleep(5)
            resposta_plexi = requests.request('GET', response_url, headers=headers_plexi_get)
            print(resposta_plexi.status_code)
            print("Aguarde 5 segundos para uma nova verificação!")

        return resposta_plexi.json()