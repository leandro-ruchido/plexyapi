# from flask_restful import Resource, reqparse
# from models.plexyresponse import PlexyresponseModel
# from models.consultas import ConsultasModel
#
# class PlexyResponseMG(Resource):
#     atributos = reqparse.RequestParser()
#     # atributos.add_argument('id_consulta', type=int, primary_key=True, auto_increment=True)
#     atributos.add_argument('placa', type=str)
#     atributos.add_argument('renavam', type=str)
#     atributos.add_argument('placaAnterior', type=str)
#     atributos.add_argument('municipio', type=str)
#     atributos.add_argument('municipioAnterior', type=str)
#     atributos.add_argument('marca', type=str)
#     atributos.add_argument('anoFabricacao', type=str)
#     atributos.add_argument('anoModelo', type=str)
#     atributos.add_argument('ipvaPago', type=str)
#     atributos.add_argument('parcelaIpva', type=str)
#     atributos.add_argument('seguroPago', type=str)
#     atributos.add_argument('parcelaSeguro', type=str)
#     atributos.add_argument('seguroAnteriorPago', type=str)
#     atributos.add_argument('taxaLicenciamentoPaga', type=str)
#     atributos.add_argument('dataLicenciamento', type=str)
#     atributos.add_argument('situacaoLicenciamento', type=str)
#     atributos.add_argument('numeroAr', type=str)
#     atributos.add_argument('comunicado', type=str)
#     atributos.add_argument('mensagemPendencias', type=str)
#     atributos.add_argument('impedimentosRestricoes', action='append', type=dict)
#     atributos.add_argument('mensagemAutuacoes', type=str)
#     atributos.add_argument('autuacoes', action='append', type=dict)
#     atributos.add_argument('mensagemMultas', type=str)
#     atributos.add_argument('multas', action='append', type=dict)
#
#     def post(self):
#         print('=============================================================================')
#         print('DADOS RECEBIDOS FORMATO JSON')
#         dados = PlexyResponseMG.atributos.parse_args()
#         plexy_objeto = PlexyresponseModel(**dados)
#         request_dados = plexy_objeto.json()
#
#         novos_impedimentos = []
#         novas_autuacoes = []
#         novas_multas = []
#         nova_consulta = []
#         nova_consulta.append(request_dados['id_consulta'])
#         nova_consulta.append(request_dados['placa'] if request_dados['placa'] else 'None')
#         nova_consulta.append(request_dados['renavam'] if request_dados['renavam'] else 'None')
#         # nova_consulta.append(request_dados['placaAnterior'] if request_dados['placaAnterior'] else 'None')
#         # nova_consulta.append(request_dados['municipio'] if request_dados['municipio'] else 'None')
#         # nova_consulta.append(request_dados['municipioAnterior'] if request_dados['municipioAnterior'] else 'None')
#         # nova_consulta.append(request_dados['marca'] if request_dados['marca'] else 'None')
#         # nova_consulta.append(request_dados['anoFabricacao'] if request_dados['anoFabricacao'] else 'None')
#         # nova_consulta.append(request_dados['anoModelo'] if request_dados['anoModelo'] else 'None')
#         # nova_consulta.append(request_dados['ipvaPago'] if request_dados['ipvaPago'] else 'None')
#         # nova_consulta.append(request_dados['parcelaIpva'] if request_dados['parcelaIpva'] else 'None')
#         # nova_consulta.append(request_dados['seguroPago'] if request_dados['seguroPago'] else 'None')
#         # nova_consulta.append(request_dados['parcelaSeguro'] if request_dados['parcelaSeguro'] else 'None')
#         # nova_consulta.append(request_dados['seguroAnteriorPago'] if request_dados['seguroAnteriorPago'] else 'None')
#         # nova_consulta.append(request_dados['taxaLicenciamentoPaga'] if request_dados['taxaLicenciamentoPaga'] else 'None')
#         # nova_consulta.append(request_dados['dataLicenciamento'] if request_dados['dataLicenciamento'] else 'None')
#         # nova_consulta.append(request_dados['situacaoLicenciamento'] if request_dados['situacaoLicenciamento'] else 'None')
#         # nova_consulta.append(request_dados['numeroAr'] if request_dados['numeroAr'] else 'None')
#         # nova_consulta.append(request_dados['comunicado'] if request_dados['comunicado'] else 'None')
#         # nova_consulta.append(request_dados['mensagemPendencias'] if request_dados['mensagemPendencias'] else 'None')
#         # nova_consulta.append(request_dados['impedimentosRestricoes'])
#         # nova_consulta.append(request_dados['mensagemAutuacoes'] if request_dados['mensagemAutuacoes'] else 'None')
#         # nova_consulta.append(request_dados['autuacoes'])
#         # nova_consulta.append(request_dados['mensagemMultas'] if request_dados['mensagemMultas'] else 'None')
#         # nova_consulta.append(request_dados['multas'])
#
#         # print(request_dados)
#         # print('=============================================================================')
#         # for campo in request_dados:
#         #     if campo == 'impedimentosRestricoes':
#         #         impedimentos = request_dados[campo]
#         #         if impedimentos is not None:
#         #             for impedimento in impedimentos:
#         #                 novo_impedimento = []
#         #                 if novo_impedimento is not None:
#         #                     nova_consulta.append(novo_impedimento)
#         #     if campo == 'autuacoes':
#         #         autuacoes = request_dados[campo]
#         #         if autuacoes is not None:
#         #             for autuacao in autuacoes:
#         #                 nova_autuacao = []
#         #                 nova_autuacao.append(autuacao['orgao'])
#         #                 nova_autuacao.append(autuacao['placa'])
#         #                 nova_autuacao.append(autuacao['descricaoSituacao'])
#         #                 nova_autuacao.append(autuacao['marca'])
#         #                 nova_autuacao.append(autuacao['codigo'])
#         #                 nova_autuacao.append(autuacao['data'])
#         #                 nova_autuacao.append(autuacao['hora'])
#         #                 nova_autuacao.append(autuacao['descricaoInfracao'])
#         #                 nova_autuacao.append(autuacao['endereco'])
#         #                 nova_autuacao.append(autuacao['municipio'])
#         #                 nova_autuacao.append(autuacao['dataInclusaoInfracao'])
#         #                 nova_autuacao.append(autuacao['dataLimiteDefesa'])
#         #                 nova_autuacao.append(autuacao['aitNotificacao'])
#         #                 nova_autuacao.append(autuacao['numeroProcessamento'])
#         #                 if nova_autuacao is not None:
#         #                     novas_autuacoes.append(nova_autuacao)
#         #                     del nova_autuacao
#         #             nova_consulta.append(novas_autuacoes)
#         #     if campo == 'multas':
#         #         multas = request_dados[campo]
#         #         if multas is not None:
#         #             for multa in multas:
#         #                 nova_multa = []
#         #                 nova_multa.append(multa['orgao'])
#         #                 nova_multa.append(multa['placa'])
#         #                 nova_multa.append(multa['descricaoSituacao'])
#         #                 nova_multa.append(multa['marca'])
#         #                 nova_multa.append(multa['codigo'])
#         #                 nova_multa.append(multa['data'])
#         #                 nova_multa.append(multa['hora'])
#         #                 nova_multa.append(multa['descricaoInfracao'])
#         #                 nova_multa.append(multa['endereco'])
#         #                 nova_multa.append(multa['municipio'])
#         #                 nova_multa.append(multa['dataInclusaoInfracao'])
#         #                 nova_multa.append(multa['dataLimiteRecurso'])
#         #                 nova_multa.append(multa['aitNotificacao'])
#         #                 nova_multa.append(multa['numeroProcessamento'])
#         #                 nova_multa.append(multa['valor'])
#         #                 if nova_multa is not None:
#         #                     novas_multas.append(nova_multa)
#         #                     del nova_multa
#         #             novaonsulta.append(novas_multas)
#         print('NOVA CONSULTA')
#         # print(nova_consulta)
#         try:
#             print(nova_consulta)
#             ConsultasModel.salvar_consulta(nova_consulta)
#             return {'message': 'Data received sucessfully!'}, 201
#         except:
#             return {'message': 'Internal error ocurred!'}, 500
#         print('=============================================================================')
